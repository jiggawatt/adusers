#queries DC for all AD users in users container, excludes non-human/admin accounts
Import-Module ActiveDirectory
$usrCont = Get-ADDomain | Select-Object -Property UsersContainer
$domainName = Get-ADDomain | Select-Object -Property Name
Get-ADUser -Filter * -SearchBase $usrCont.UsersContainer -Properties LastLogonDate, MemberOf  |
 Where-Object { $_.Name -notlike "*scanner*" -and $_.Name -notlike "*scanning*" -and $_.Name -notlike "*admin*" -and $_.Name -notlike "*QB*"-and $_.Name -notlike "*default*" } |
  Select-Object -Property UserPrincipalName, Name, LastLogonDate, @{name="MemberOf";expression={$_.memberof -join ","}} |
   Export-CSV -Path "C:\activeusers2-$($domainName.Name).csv"

